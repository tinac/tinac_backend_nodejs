module.exports = (sequelize, Sequelize) => {
    let User = sequelize.define('User', {
        username: Sequelize.STRING,
        password: Sequelize.STRING,
        degree: Sequelize.INTEGER
    }, {
        // add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute
        // deletedAt to the current date (when deletion was done). paranoid will
        //only work if timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore
        // style so updatedAt will be updated_at
        underscored: true,

        // define the table's name
        tableName: 'users',
    });

    // http://docs.sequelizejs.com/manual/tutorial/associations.html
    User.associate = function(models) {
        models.User.belongsToMany(models.Subject, {
            as: "Subjects",
            through: "user_subjects",
            foreignKey: 'user_id',            
        });
    };

    return User;
};
