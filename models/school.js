// nombre	universidad	provincia	ciudad	direccion	numero	codigo_postal
module.exports = (sequelize, Sequelize) => {
    let School = sequelize.define('School', {
        name: Sequelize.STRING,
        university: Sequelize.STRING,
        province: Sequelize.STRING,
        city: Sequelize.STRING,
        address: Sequelize.STRING,
        number: Sequelize.INTEGER,
        postal_code: Sequelize.INTEGER
    }, {
        // add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute
        // deletedAt to the current date (when deletion was done). paranoid will
        //only work if timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore
        // style so updatedAt will be updated_at
        underscored: true,

        // define the table's name
        tableName: 'schools',
    });

    // http://docs.sequelizejs.com/manual/tutorial/associations.html
    School.associate = function(models) {
        models.School.belongsTo(models.University, {
            foreignKey: 'university',
            targetKey: 'id'
        });
        models.School.hasMany(models.Degree, {
          foreignKey: 'school',
          sourceKey: 'id'
        });
    };

    return School;
};
