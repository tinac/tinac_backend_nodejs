// nombre	universidad	provincia	ciudad	direccion	numero	codigo_postal
module.exports = (sequelize, Sequelize) => {
    let Degree = sequelize.define('Degree', {
        code:{
          type: Sequelize.INTEGER,
          primaryKey: true
        },
        name: Sequelize.STRING,
        school: Sequelize.INTEGER,
    }, {
        // add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute
        // deletedAt to the current date (when deletion was done). paranoid will
        //only work if timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore
        // style so updatedAt will be updated_at
        underscored: true,

        // define the table's name
        tableName: 'degrees',
    });

    // http://docs.sequelizejs.com/manual/tutorial/associations.html
    Degree.associate = function(models) {
        models.Degree.belongsTo(models.School, {
            foreignKey: 'school',
            targetKey: 'id'
        });
        models.Degree.hasMany(models.Subject, {
          foreignKey: 'degree',
          sourceKey: 'code'
        });
    };

    return Degree;
};
