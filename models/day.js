// universidad	sede_principal	comunidad_autonoma	año_fundacion	tipo
module.exports = (sequelize, Sequelize) => {
    let Day = sequelize.define('Day', {
        day: Sequelize.INTEGER,
        month: Sequelize.INTEGER,
        year: Sequelize.INTEGER,
        name: Sequelize.STRING,
        effective_name: Sequelize.STRING,
        holiday: Sequelize.INTEGER
    }, {
        // add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute
        // deletedAt to the current date (when deletion was done). paranoid will
        //only work if timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore
        // style so updatedAt will be updated_at
        underscored: true,

        // define the table's name
        tableName: 'days',
    });

    return Day;
};
