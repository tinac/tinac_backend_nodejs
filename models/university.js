// universidad	sede_principal	comunidad_autonoma	año_fundacion	tipo
module.exports = (sequelize, Sequelize) => {
    let University = sequelize.define('University', {
        name: Sequelize.STRING,
        headquarters: Sequelize.STRING,
        autonomous_community: Sequelize.STRING,
        year_of_foundation: Sequelize.INTEGER,
        type: Sequelize.STRING
    }, {
        // add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute
        // deletedAt to the current date (when deletion was done). paranoid will
        //only work if timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore
        // style so updatedAt will be updated_at
        underscored: true,

        // define the table's name
        tableName: 'universities',
    });

    // http://docs.sequelizejs.com/manual/tutorial/associations.html
    University.associate = function(models) {
        models.University.hasMany(models.School, {
            foreignKey: 'university',
            sourceKey: 'id'
        });
    };

    return University;
};
