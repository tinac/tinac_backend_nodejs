// nombre	universidad	provincia	ciudad	direccion	numero	codigo_postal
module.exports = (sequelize, Sequelize) => {
    let Schedule = sequelize.define('Schedule', {
        subject: Sequelize.INTEGER,
        type: Sequelize.STRING,
        building: Sequelize.STRING,
        class: Sequelize.STRING,
        group: Sequelize.INTEGER,
        week: Sequelize.INTEGER,
        day: Sequelize.STRING,
        start_hour: Sequelize.STRING,
        finish_hour: Sequelize.STRING
    }, {
        // add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute
        // deletedAt to the current date (when deletion was done). paranoid will
        //only work if timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore
        // style so updatedAt will be updated_at
        underscored: true,

        // define the table's name
        tableName: 'schedules',
    });

    // http://docs.sequelizejs.com/manual/tutorial/associations.html
    Schedule.associate = function(models) {
        models.Schedule.belongsTo(models.Subject, {
            foreignKey: 'subject',
            targetKey: 'code'
        });
    };

    return Schedule;
};
