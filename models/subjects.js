// nombre	universidad	provincia	ciudad	direccion	numero	codigo_postal
module.exports = (sequelize, Sequelize) => {
    let Subject = sequelize.define('Subject', {
        course: Sequelize.INTEGER,
        code: {
          type: Sequelize.INTEGER,
          primaryKey: true
        },
        name: Sequelize.STRING,
        degree: Sequelize.INTEGER,
        type: Sequelize.STRING,
        credits: Sequelize.INTEGER,
        period: Sequelize.STRING,
        availability: Sequelize.STRING,
        limit: Sequelize.INTEGER,
        language: Sequelize.STRING,
    }, {
        // add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute
        // deletedAt to the current date (when deletion was done). paranoid will
        //only work if timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore
        // style so updatedAt will be updated_at
        underscored: true,

        // define the table's name
        tableName: 'subjects',
    });

    // http://docs.sequelizejs.com/manual/tutorial/associations.html
    Subject.associate = function(models) {
        models.Subject.belongsTo(models.Degree, {
            foreignKey: 'degree',
            targetKey: 'code'
        });
        models.Subject.hasMany(models.Schedule, {
          foreignKey: 'subject',
          sourceKey: 'code'
        });
        models.Subject.belongsToMany(models.User, {
            as: "Students",
            through: "user_subjects",
            foreignKey: 'subject_id',            
        });
    };

    return Subject;
};
