# API Interface

## Http headers requirements

### General  
`Accept: application/json`  
`Content-Type: application/json`

### Login  
`Authorization: Basic XXXXX`  
\* _XXXX is - b64(username:password)_

### Before Login
`x-auth-token: XXXX`  
\* _XXXX is the token given by login_

## Available URLs

### User Access
- Login current user (get the token)  
POST `/login/`  
\* _requires authentication header_

- Register the user  
POST `/register/`  
\* _body should be {"username": yourusername, "password": yourpassword}_

## For Information Retrieval

- Universities  
GET `/university`

- One University  
GET `/university/:id`

- School  
GET `/school/:id`

- Degree  
GET `/degree/:code`

- Subject  
GET `/subject/:code`

- Get all school days classes  
GET `/day`

## For Data Insertion
- Enrol the current user to a subject  
POST `user_subject/:subject_code`

# Requirements
- Node.js >= v8.11.1
