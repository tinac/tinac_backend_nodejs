module.exports = {
    development:{
        db: 'database',
        dialect: 'sqlite',
        username: 'username',
        password: 'password',
        host: 'localhost',
        storage: 'db.sqlite3'
    }
}
