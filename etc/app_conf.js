module.exports = {
    'port':         process.env.TINAC_API_PORT      || 8888,
    'address':      process.env.TINAC_API_ADDRESS   || "0.0.0.0",
    'secret_key': process.env.TINAC_TOKEN_PASSWD  || "29r0f8eg04wev8erg0wwe0fgt48resdfwe7r98v7er8g7w23erwfuiys8" // Manually generated, used to create JWTs
}
