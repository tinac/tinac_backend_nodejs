var express = require('express');
var router = express.Router();
const models = require("../models");

/* GET day
 * Returns all the days with classes of the year
 */
router.get('/', function(req, res, next) {
    let user_id = req.token['id'];
    let result = []

    //TODO: MAKE THIS LOOK BETTER
    models.Day.findAll().then(days => {
        models.User.find({
            where: {
                id: {
                    [models.Sequelize.Op.eq]: user_id
                }
            }
        }).then(user => {
            user.getSubjects().then(subjects => {
                subjects_codes = [];
                subjects.forEach(subject => {
                    subjects_codes.push(subject.dataValues['code']);
                });

                models.Schedule.findAll({
                    where: {
                        subject: {
                            [models.Sequelize.Op.in]: subjects_codes
                        }
                    }
                }).then(schedules => {
                    days.forEach(day => {
                        day_tmp = {
                            day: day.dataValues['day'],
                            month: day.dataValues['month'],
                            year: day.dataValues['year'],
                            classes: []
                        }
                        // day_tmp = JSON.parse(JSON.stringify(day));
                        subjects.forEach(subject => {
                            schedules.forEach(schedule => {
                                //If that day there is a class for that subject
                                if(schedule.dataValues['day'] === day.dataValues['effective_name'] &&
                                    schedule.dataValues['subject'] === subject.dataValues['code']){
                                    class_tmp = {
                                        name: subject.dataValues['name'],
                                        schedule: {
                                            type: schedule.dataValues['type'],
                                            group: schedule.dataValues['group'],
                                            start_hour: schedule.dataValues['start_hour'],
                                            finish_hour: schedule.dataValues['finish_hour']
                                        }
                                    }
                                    day_tmp.classes.push(class_tmp);
                                }
                            });
                        });
                        result.push(day_tmp)
                    });
                    res.statusCode = 200;
                    res.json({ message: 'Success', data: result });
                }).catch(err => {
                    res.statusCode = 500;
                    res.json({message: "database query failed4"});
                });
            }).catch(err => {
                res.statusCode = 500;
                res.json({message: "Something goes wrong3"});
            });;
        }).catch(err => {
            res.statusCode = 500;
            res.json({message: "database query failed2"});
        });
    }).catch(err => {
        res.statusCode = 500;
        res.json({message: "database query failed1"});
    });    
});

module.exports = router;
