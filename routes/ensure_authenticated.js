const express = require('express');
var jwt = require('jsonwebtoken');
const router = express.Router();
const conf = require('../etc/app_conf');
const models = require("../models");
const bcrypt = require("bcrypt");

module.exports = function(req, res, next){
    let user_token = req.headers["x-auth-token"];
    //header noes not contains token
    if(user_token){
        jwt.verify(user_token, conf.secret_key, function(err, decoded_token){
            if(err){
                res.statusCode = 401;
                res.json({message: "invalid token"})
            }
            else{
                // Arrach the token to the request.
                req.token = decoded_token;
                next();
            }
        });
    }
    else{
        res.statusCode = 401;
        res.setHeader("WWW-Authenticate", "Basic realm='Login required'");
        res.json({message: "token not found"});
    }
};
