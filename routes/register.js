const express = require('express');
const router = express.Router();
const conf = require('../etc/app_conf');
const models = require("../models");
const bcrypt = require('bcrypt');

/* GET register */
router.post('/', function (req, res, next) {
    const username = req.body.username;
    const password = req.body.password;

    //Username and passoword not null or empty
    if(username && password){
        models.User.find({
            where: {
                username: {
                    [models.Sequelize.Op.eq]: username
                }
            }
        }).then(user => {
            //Username already exists
            if(user){
                res.statusCode = 401;
                res.json({ message: "username already exists" });
            }
            else{
                //Password hashing
                bcrypt.hash(password, 10, function(err, hash) {
                    models.User.create({username: username, password: hash})
                    .then(user => {
                        res.statusCode = 200;
                        res.json({ message: "you have been registered successfully" });
                    }).catch(err => {
                        res.statusCode = 500;
                        res.json({ message: "could not crete the user" });
                    });
                });
            }
        }).catch(err => {
            res.statusCode = 500;
            res.json({ message: "database query failed" });
        });
    }
    else{
        res.statusCode = 401;
        res.json({ message: "empty username or password" });
    }
});

module.exports = router;
