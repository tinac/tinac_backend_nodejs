var express = require('express');
var router = express.Router();
const models = require("../models");

/* GET user_subject
 * Returns all the subjects that the students study actually
*/
router.get('/', function(req, res){
    let user_id = req.token['id'];
    let result = [];

    models.User.find({
        where: {
            id: {
                [models.Sequelize.Op.eq]: user_id
            }
        }
    }).then(user => {
        user.getSubjects().then(subjects => {
            subjects.forEach(subject => {
                subject_tmp = {
                    id: subject.dataValues['code'],
                    name: subject.dataValues['name'],
                    url: "/subject/" + subject.dataValues['code']
                }
                result.push(subject_tmp);
            });
            res.statusCode = 200;
            res.json({ message: 'Success', data: result });
        }).catch(err => {
            res.statusCode = 500;
            res.json({message: "Something goes wrong"});
        });;

    }).catch(err => {
        res.statusCode = 500;
        res.json({message: "database query failed"});
    });
});

/* POST user_subject
 * Add a subject to a student
*/
router.post('/', function(req, res){
    let user_id = req.token['id'];
    let subject_code = req.body.subject_code;

    models.Subject.find({
      where: {
        code : {
          [models.Sequelize.Op.eq]: subject_code
        }
      }
    }).then(subject => {
        models.User.find({
            where: {
                id: {
                    [models.Sequelize.Op.eq]: user_id
                }
            }
        }).then(user => {
            // Add the subject to the user
            user.addSubject(subject).then(() => {
                res.statusCode = 200;
                res.json({ message: 'Success' });
            }).catch(err => {
                res.statusCode = 500;
                res.json({message: "Something goes wrong"});
            });
        }).catch(err => {
            res.statusCode = 500;
            res.json({message: "database query failed"});
        });
    }).catch(err => {
        res.statusCode = 500;
        res.json({message: "database query failed"});
    });
});

/* DELETE user_subject
 * Remove a subject from a student
*/
router.delete('/:subject_code', function(req, res){
    let user_id = req.token['id'];
    let subject_code = req.params.subject_code;

    models.Subject.find({
      where: {
        code : {
          [models.Sequelize.Op.eq]: subject_code
        }
      }
    }).then(subject => {
        models.User.find({
            where: {
                id: {
                    [models.Sequelize.Op.eq]: user_id
                }
            }
        }).then(user => {
            // Remove the subject from the user
            user.removeSubject(subject).then(() => {
                res.statusCode = 200;
                res.json({ message: 'Success' });
            }).catch(err => {
                res.statusCode = 500;
                res.json({message: "Something goes wrong"});
            });
        }).catch(err => {
            res.statusCode = 500;
            res.json({message: "database query failed"});
        });
    }).catch(err => {
        res.statusCode = 500;
        res.json({message: "database query failed"});
    });
});


module.exports = router;
