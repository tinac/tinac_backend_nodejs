var express = require('express');
var router = express.Router();
const models = require("../models");

/* GET university */
router.get('/', function(req, res) {
    let result = []
    models.University.findAll().then(universities => {
        universities.forEach(university => {
            // console.log(university);
            uni_tmp = {
                id: university.dataValues['id'],
                name: university.dataValues['name'],
                url: "/university/" +  university.dataValues['id']
            }
            result.push(uni_tmp);
        });
        res.statusCode = 200;
        res.json({ message: 'Success', data: result });
    }).catch(err => {
        res.statusCode = 500;
        res.json({ message: "database query failed" });
    });
});

/* GET universities with parameter*/
router.get('/:id', function(req, res){
    let id = req.params.id
    let result = []
    models.University.find({
        where: {
            id: {
                [models.Sequelize.Op.eq]: id
            }
        }
    }).then(university => {
        // console.log(university);
        university_tmp = university.dataValues;
        university_tmp.schools = [];
        models.School.findAll({
            where: {
                university: {
                    [models.Sequelize.Op.eq]: university.dataValues['id']
                }
            }
        }).then(schools => {
            schools.forEach(school => {
                school_tmp = {
                    id: school.dataValues['id'],
                    name: school.dataValues['name'],
                    url: "/school/" + school.dataValues['id']
                }
                university_tmp.schools.push(school_tmp);
            });
            result.push(university_tmp);
            res.statusCode = 200;
            res.json({ message: 'Success', data: result });
        }).catch(err => {
            res.statusCode = 500;
            res.json({message: "database query failed"});
        });
    }).catch(err => {
        res.statusCode = 500;
        res.json({message: "database query failed"});
    });
});

module.exports = router;
