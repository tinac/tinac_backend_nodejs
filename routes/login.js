const express = require('express');
var jwt = require('jsonwebtoken');
const router = express.Router();
const conf = require('../etc/app_conf');
const models = require("../models");
const bcrypt = require("bcrypt");

/* GET login. */
router.post("/", function (req, res, next) {
    // auth is in base64(username:password)  so we need to decode the base64
    let auth = req.headers["authorization"];

    if(!auth) {
        // No Authorization header was passed in so it's the first time the browser hit us
        // Sending a 401 will require authentication, we need to send the 'WWW-Authenticate' to tell them the sort of authentication to use
        // Basic auth is quite literally the easiest and least secure, it simply gives back  base64( username + ":" + password ) from the browser
        res.statusCode = 401;
        res.setHeader("WWW-Authenticate", "Basic realm='Login required'");
        res.json({message: "could not verify"});
    }
    else{
        // Split on a space, the original auth looks like  "Basic Y2hhcmxlczoxMjM0NQ==" and we need the 2nd part
        var tmp = auth.split(' ');

        // create a buffer and tell it the data coming in is base64 and
        // read it back out as a string
        var plain_auth = new Buffer(tmp[1], 'base64').toString();

        // split on a ':'
        var credentials = plain_auth.split(':');
        var username = credentials[0];

        //get a slice from the second element to the end if password contains ':'
        var password = credentials.slice(1, credentials.length).join(":");

        models.User.find({
          where: {
            username: {
              [models.Sequelize.Op.eq]: username
            }
          }
        }).then(user => {
            //Username does not exist
            if(!user){
                res.statusCode = 401;
                res.setHeader("WWW-Authenticate", "Basic realm='Login required'");
                res.json({message: "no username"});
            }
            //User exists
            else{
                let user_password = user.dataValues["password"];
                bcrypt.compare(password, user_password, function(err, match){
                    //password matches
                    if(match){
                        //1 day expiration token
                        let jwt_token = jwt.sign({
                            id: user.dataValues['id'],
                            username: user.dataValues['username']
                            },
                            conf.secret_key,
                            {expiresIn: 60 * 60 * 24});

                        res.statusCode = 200;
                        res.json({ token: jwt_token });
                    }
                    else{
                        res.statusCode = 401;
                        res.setHeader("WWW-Authenticate", "Basic realm='Login required'");
                        res.json({message: "password does not match"});
                    }
                });
            }
        }).catch(err => {
            res.statusCode = 500;
            res.json({ message: "database query failed" });
        });
    }
});

module.exports = router;
