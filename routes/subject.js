var express = require('express');
var router = express.Router();
const models = require("../models");

/* GET subject */
router.get('/:code', function(req, res){
    let code = req.params.code
    let result = []
    models.Subject.find({
        where: {
            code: {
                [models.Sequelize.Op.eq]: code
            }
        }
    }).then(subject => {
        subject_tmp = subject.dataValues;
        subject_tmp.schedules = [];
        models.Schedule.findAll({
            where: {
                subject: {
                    [models.Sequelize.Op.eq]: subject.dataValues['code']
                }
            }
        }).then(schedules => {
            schedules.forEach(schedule => {
                subject_tmp.schedules.push(schedule.dataValues);
            });
            result.push(subject_tmp);
            res.statusCode = 200;
            res.json({ message: 'Success', data: result });
        }).catch(err => {
            res.statusCode = 500;
            res.json({message: "database query failed"});
        });
    }).catch(err => {
        res.statusCode = 500;
        res.json({message: "database query failed"});
    });
});

module.exports = router;
