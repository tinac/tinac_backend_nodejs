var express = require('express');
var router = express.Router();
const models = require("../models");

/* GET degree */
router.get('/:code', function(req, res){
    let code = req.params.code
    let result = []
    models.Degree.find({
        where: {
            code: {
                [models.Sequelize.Op.eq]: code
            }
        }
    }).then(degree => {
        // console.log(university);
        degree_tmp = degree.dataValues;
        degree_tmp.subjects = [];
        models.Subject.findAll({
            where: {
                degree: {
                    [models.Sequelize.Op.eq]: degree.dataValues['code']
                }
            }
        }).then(subjects => {
            subjects.forEach(subject => {
                subject_tmp = {
                    id: subject.dataValues['code'],
                    name: subject.dataValues['name'],
                    url: "/subject/" + subject.dataValues['code']
                }
                degree_tmp.subjects.push(subject_tmp);
            });
            result.push(degree_tmp);
            res.statusCode = 200;
            res.json({ message: 'Success', data: result });
        }).catch(err => {
            res.statusCode = 500;
            res.json({message: "database query failed"});
        });
    }).catch(err => {
        res.statusCode = 500;
        res.json({message: "database query failed"});
    });
});

module.exports = router;
