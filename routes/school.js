var express = require('express');
var router = express.Router();
const models = require("../models");

/* GET school */
router.get('/:id', function(req, res){
    let id = req.params.id
    let result = []
    models.School.find({
        where: {
            id: {
                [models.Sequelize.Op.eq]: id
            }
        }
    }).then(school => {
        // console.log(university);
        school_tmp = school.dataValues;
        school_tmp.degrees = [];
        models.Degree.findAll({
            where: {
                school: {
                    [models.Sequelize.Op.eq]: school.dataValues['id']
                }
            }
        }).then(degrees => {
            degrees.forEach(degree => {
                degree_tmp = {
                    id: degree.dataValues['code'],
                    name: degree.dataValues['name'],
                    url: "/degree/" + degree.dataValues['code']
                }
                school_tmp.degrees.push(degree_tmp);
            });
            result.push(school_tmp);
            res.statusCode = 200;
            res.json({ message: 'Success', data: result });
        }).catch(err => {
            res.statusCode = 500;            
            res.json({message: "database query failed"});
        });
    }).catch(err => {
        res.statusCode = 500;
        res.json({message: "database query failed"});
    });
});

module.exports = router;
