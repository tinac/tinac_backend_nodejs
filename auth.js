/*
    TiNAC
    This script defines how Passport will validate an user
    using the Local Strategy.
 */
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const models = require('models');
const User = models.User;
const sha512 = require("js-sha512").sha512;

passport.use(new LocalStrategy(
    function (username, password, done) {
        const passwd = sha512(password);
        User.find({
            where: {
                username: {
                    [models.Sequelize.Op.eq]: username
                }
            }
        }).then(user => {
            if (user.passwd === passwd) { // Password match
                return done(null, user);
            }
            // Password didn't match
            return done(null, false, {message: "Incorrect username/password."})
        }).catch(err => done(err)); // There was an error
    }
));