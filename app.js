// https://code.tutsplus.com/es/tutorials/using-passport-with-sequelize-and-mysql--cms-27537

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fs = require('fs');
const models = require("./models");
const ensureAuthenticated = require("./routes/ensure_authenticated")

// TODO: Esto debe estar directamente en la base de datos.....

subject_code = {
    "arquitectura":         470,
    "diseno_industrial":    558,
    "electrica":            430,
    "electronica":          440,
    "industriales":         436,
    "informatica":          439,
    "mecanica":             434,
    "quimica":              435,
    "teleco":               438

}

models.sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
})
.catch(err => {
    console.error('Unable to connect to the database:', err);
});

let sync = function(){
    //Sync Database
    models.sequelize.sync({force:true}).then(function() {

        // adds universities and schools
        let universities = JSON.parse(fs.readFileSync('./data/universities/json/universidades_de_espana.json', 'utf8'));
        universities.forEach(university => {
            // console.debug(universidad);
            models.University.create(university).then(uni => {
                // console.debug(uni.get({
                //     plain: true
                // }));
            }).catch(err => {
                console.log(err)
            });
        });

        let schools = JSON.parse(fs.readFileSync('./data/schools/json/facultades_de_espana.json', 'utf8'));
        schools.forEach(school => {
            console.debug(school);
            models.School.create(school).then(sch => {
                // console.debug(facu.get({
                //     plain: true
                // }));
            }).catch(err => {
                console.log(err)
            });
        });

        //Adds all degrees from files in folder degrees/json
        fs.readdir('./data/degrees/json/', (err, files) => {
            files.forEach(file => {
                console.log(file)
                let degrees = JSON.parse(fs.readFileSync('./data/degrees/json/' + file, 'utf8'));
                degrees.forEach(degree => {
                    // console.debug(universidad);
                    models.Degree.create(degree).then(deg => {
                        // console.debug(uni.get({
                        //     plain: true
                        // }));
                    }).catch(err => {
                        console.log(err)
                    });
                });
            });
        });

        //Adds all subjects from files in folder degrees/json
        fs.readdir('./data/subjects/json/', (err, files) => {
            files.forEach(file => {
                console.log(file)
                let subjects = JSON.parse(fs.readFileSync('./data/subjects/json/' + file, 'utf8'));
                subjects.forEach(subject => {
                    //Adds the degree code
                    subject['degree'] = subject_code[file.split('.')[0]]
                    // console.debug(subject);
                    models.Subject.create(subject).then(sub => {
                        // console.debug(uni.get({
                        //     plain: true
                        // }));
                    }).catch(err => {
                        console.log(err)
                    });
                });
            });
        });

        //Adds all degrees from files in folder degrees/json
        fs.readdir('./data/schedules/json/', (err, files) => {
            files.forEach(file => {
                console.log(file)
                let schedules = JSON.parse(fs.readFileSync('./data/schedules/json/' + file, 'utf8'));
                schedules.forEach(schedule => {
                    // console.debug(universidad);
                    models.Schedule.create(schedule).then(sch => {
                        // console.debug(uni.get({
                        //     plain: true
                        // }));
                    }).catch(err => {
                        console.log(err)
                    });
                });
            });
        });

        // adds universities and schools
        let days = JSON.parse(fs.readFileSync('./data/days/json/calendario_eina.json', 'utf8'));
        days.forEach(day => {
            // console.debug(universidad);
            models.Day.create(day).then(day => {
                // console.debug(uni.get({
                //     plain: true
                // }));
            }).catch(err => {
                console.log(err)
            });
        });

        console.log('Nice! Database looks fine');
    }).catch(function(err) {
        // console.log(err, "Something went wrong with the Database Update!")
        console.log("Something went wrong with the Database Update!");
    });
}

let queries = function(){
  // Get all subjects from "Ingeniería Informática"
  models.Degree.find({
      where: {
          name: {
              [models.Sequelize.Op.like]: '%inform%'
          }
      }
  }).then(degree => {
      let code = degree.dataValues['code']
      console.log(code)
      models.Subject.findAll({
          where: {
              degree :{
                  [models.Sequelize.Op.eq]: code
              }
          }
      }).then(subjects => {
          subjects.forEach(subject => {
              console.log(subject.dataValues['name']);
          });
      });
  });

  models.Subject.find({
    where: {
      name : {
        [models.Sequelize.Op.like] : '%comerciales%'
      }
    }
  }).then(subject => {
    let name = subject.dataValues['name'];
    let code = subject.dataValues['code'];
    models.Schedule.findAll({
      where: {
        subject: {
          [models.Sequelize.Op.eq]: code
        }
      }
    }).then(schedules => {
      schedules.forEach(schedule => {
        // console.log(schedule)
        let type = schedule.dataValues['type']
        let day = schedule.dataValues['day']
        let start_hour = schedule.dataValues['start_hour']
        let finish_hour = schedule.dataValues['finish_hour']
        console.log(name + "(" + type + "):" + day + " de " + start_hour + " a " + finish_hour)
      });
    });
  });
}

let addSubject = function(user, subject){
    models.Subject.find({
      where: {
        name : {
          [models.Sequelize.Op.like] : "%" + subject + "%"
        }
      }
    }).then(subject => {
        models.User.find({
            where: {
                username: {
                    [models.Sequelize.Op.like] : "%" + user + "%"
                }
            }
        }).then(user => {
            user.addSubject(subject);
        })
    })
}

// sync();
// queries();
// addSubject("vlad", "comerciales");

const index = require('./routes/index');
const login = require('./routes/login');
const register = require('./routes/register');

//Data routes
const university = require('./routes/university');
const school = require('./routes/school');
const degree = require('./routes/degree');
const subject = require('./routes/subject');

const user_subject = require('./routes/user_subject');

const day = require('./routes/day');

const app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));


app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Authorization, Content-Length, X-Requested-With');

    //intercepts OPTIONS method
    if (req.method === 'OPTIONS') {
      //respond with 200
      res.send(200);
    }
    else {
    //move on
      next();
    }
});

app.use('/login', login);
app.use('/register', register);

//Middleware that ensures that token is valid
app.use(ensureAuthenticated);

app.use('/', index);

//Data routes
app.use('/university', university);
app.use('/school', school);
app.use('/degree', degree);
app.use('/subject', subject);
app.use('/user_subject', user_subject);
app.use('/day', day);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.statusCode = err.status || 500;
  res.json({ message: 'error' });
});

module.exports = app;
